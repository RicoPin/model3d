Model 3D Viewer
===============

A web based [Model 3D file viewer](https://bztsrc.gitlab.io/model3d/viewer), provides the same functionality as the
[m3dview](https://gitlab.com/bztsrc/model3d/tree/master/m3dview) utility.

**NOTE**: compiles without errors, but not working ATM, throws lots of "TODO" warnings and an error in one of the libraries,
something about "numVertexes must be an integer"? Probably needs a complete emscripten stupid-proof, OpenGL3.3 only rewrite...
